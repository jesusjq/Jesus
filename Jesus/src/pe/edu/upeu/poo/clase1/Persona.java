package pe.edu.upeu.poo.clase1;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Persona {

	String nombre; 
	String apellidoPaterno;
	String apellidoMaterno;
	Double altura;
	String dni;
	String celular;
	Date fechaNacimiento;
	String email;
	String tipoSangre;
	String direccion;
	String religion;
	String sexo;
	String estadocivil;
	Boolean vive;
	
	public int getEdad(){
		SimpleDateFormat sdf= new SimpleDateFormat("yyyy");
		Date now = new Date();
		int anioActual = Integer.parseInt(sdf.format(now));
		int anioNacimiento = Integer.parseInt(sdf.format(fechaNacimiento));
		
		return anioActual - anioNacimiento;		
		}

	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", apellidoPaterno=" + apellidoPaterno + ", apellidoMaterno="
				+ apellidoMaterno + ", altura=" + altura + ", dni=" + dni + ", celular=" + celular
				+ ", fechaNacimiento=" + fechaNacimiento + ", email=" + email + ", tipoSangre=" + tipoSangre
				+ ", direccion=" + direccion + ", religion=" + religion + ", sexo=" + sexo + ", estadocivil="
				+ estadocivil + ", vive=" + vive + "]";
	}
			
	
	
}
