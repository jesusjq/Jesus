package pe.edu.upeu.poo.clase2;

public class Repaso {

	public static void main(String[] args) {

		Repaso r = new Repaso();
		int a = 10;
		// double a = 25;
		int b = 15;
		// double b = 144;

		/**
		 * suma + resta - Multiplicacion * Division / Modulo % Matematica
		 * "Match"
		 */

		// Esta instrucion me sirve para imprimir en la consola
		// System.out.println(a%b);
		// System.out.println(Math.sqrt(a+b));
		System.out.println("La Suma es: " + r.sumar(a, b));

		System.out.println("Operadores Logicas");
		

		/**
		 * And && Or || (Alt + 124) Negacion !
		 */

		System.out.println(!true);
		
		System.out.println("Calculando el IGV");
		System.out.println("IGV: "+r.calcIGV(100));
		
		//calculando masa corporal
		System.out.println("calculando IMC");
		System.out.println("Su IMC es: "+r.IMC(85, 1.74));
	}

	public int sumar(int sum1, int sum2) {
		return sum1 + sum2;
	}

	public double calcIGV(double monto) {
		double igv = 18;
		return monto * igv / 100;
	}
	public double IMC(double peso, double altura){
		
		return peso / (altura * altura);
	}
}
